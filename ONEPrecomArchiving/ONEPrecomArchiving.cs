﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using System.Configuration;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data;
using System.Threading;

namespace ONEPrecomArchiving
{
    class ONEPrecomArchiving
    {
        private void executeArchiving()
        {
            Log("executeArchiving start <" + DateTime.Now + ">");
            DateTime sixMonthsAgo = DateTime.Today.AddMonths(getMonthsBack());
            string limitDate = Convert.ToDateTime(sixMonthsAgo).ToString("yyyy-MM-dd");
            SqlConnection connection = getSqlConnection();
            Log("limitdate <" + limitDate + ">");
            deleteOrders(connection, limitDate);
            deleteMasterdata(connection, limitDate);
            deleteLogs(connection);
            Log("executeArchiving stop <" + DateTime.Now + ">");
        }

        private int getMonthsBack()
        {
            string monthsBack = getAppSetting("MonthsBack", "-6");
            int mb = -6;
            if (!monthsBack.StartsWith("-"))
            {
                monthsBack = "-" + monthsBack;
            }
            if (!Int32.TryParse(monthsBack, out mb))
            {
                mb = -6;
            }
            return mb;
        }


        private void deleteOrders(SqlConnection connection, string limitDate)
        {
            int noOfOrders = getNoOfOrders();
            DateTime startTime = DateTime.Now;
            List<string> orders = getOrdersForArchiving(connection, limitDate, noOfOrders);
            if (orders != null)
            {
                Log("Orders to delete "+orders.Count);
            }
            if (orders != null && orders.Count > 0)
            {
                int count_2 = 0;
                foreach (string order in orders)
                {
                    count_2++;
                    Log(count_2 + " - " + order);
                }
                deleteData(connection, orders);
                //testsleep();
                DateTime stopTime = DateTime.Now;
                calcNewNoOfOrders(startTime, stopTime, noOfOrders);
            }
        }

        private void deleteLogs(SqlConnection connection)
        {
            try
            {
                DateTime aYearAgo = DateTime.Today.AddMonths(-12);
                string limitDate = Convert.ToDateTime(aYearAgo).ToString("yyyy-MM-dd");
                if (connection != null && connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }
                string sql_str_file = "DELETE FROM Attention WHERE Time < '" + limitDate + "'";
                SqlCommand myCommand_attention = new SqlCommand(sql_str_file, connection);
                myCommand_attention.ExecuteNonQuery();

                sql_str_file = "DELETE FROM PMC_Log_Server WHERE EventTimeUtc < '" + limitDate + "'";
                SqlCommand myCommand_log = new SqlCommand(sql_str_file, connection);
                myCommand_log.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                Log("deleteLogs <" + e.ToString() + ">");
            }
            finally
            {
                if (connection != null && connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
        }

        private void deleteMasterdata(SqlConnection connection, string limitDate)
        {
            try
            {
                List<string> articles = getArticlesForArchiving(connection, limitDate);
                if (articles != null && articles.Count > 0)
                {
                    foreach (string articleId in articles)
                    {
                        DeleteSynchronizationMetadata(articleId, connection);
                        DeleteSynchronizationPropertyMetadata(articleId, connection);
                        DeleteDbObjectById("ArticleEntity", articleId, connection);
                    }
                }

                List<string> materials = getMaterialsForArchiving(connection, limitDate);
                if (materials != null && materials.Count > 0)
                {
                    foreach (string materialId in materials)
                    {
                        DeleteSynchronizationMetadata(materialId, connection);
                        DeleteSynchronizationPropertyMetadata(materialId, connection);
                        DeleteDbObjectById("MaterialEntity", materialId, connection);
                    }
                }

                List<string> aos = getAOsForArchiving(connection, limitDate);
                if (aos != null && aos.Count > 0)
                {
                    foreach (string aoId in aos)
                    {
                        DeleteSynchronizationMetadata(aoId, connection);
                        DeleteSynchronizationPropertyMetadata(aoId, connection);
                        DeleteDbObjectById("AONumberEntity", aoId, connection);
                    }
                }
            }
            catch (Exception e)
            {
                Log("deleteMasterdata <" + e.ToString() + ">");
            }
        }

        private void testsleep()
        {
            Random rnd = new Random();
            int card = rnd.Next(20);
            Thread.Sleep(card*1000);
        }

        private void deleteData(SqlConnection connection, List<string> orders)
        {
            try
            {
                foreach (string orderId in orders)
                {
                    DeleteOneOrderLog(orderId, connection);
                    Tuple<List<string>, List<string>> orderFiles = getOrderFiles(connection, orderId);

                    List<string> orderFileNames = orderFiles.Item1;
                    List<string> orderFileIds = orderFiles.Item2;

                    //Console.WriteLine("found <"+orderFileNames.Count+"> files for order <"+orderId+">");

                    if (orderFileNames != null && orderFileNames.Count > 0)
                    {
                        DeleteFiles(orderFileNames, orderFileIds, orderId, connection);
                    }
                    List<string> orderReports = DeleteOneReportEntity(orderId, connection);
                    DeleteSynchronizationMetadata_order(orderId, orderReports, orderFileIds, connection);
                    DeleteSynchronizationPropertyMetadata_order(orderId, orderReports, orderFileIds, connection);
                    DeleteOneOrderUserEntity(orderId, connection);
                    DeleteDbObjectById("MWL_Order", orderId, connection);
                }
            }
            catch (Exception e)
            {
                Log("deleteData <" + e.ToString() + ">");
            }
        }

        private void DeleteOneOrderUserEntity(string orderId, SqlConnection connection)
        {
            try
            {
                if (connection != null && connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }
                string sql_str = "DELETE FROM OneOrderUserEntity WHERE OrderId='" + orderId + "'";
                SqlCommand myCommand = new SqlCommand(sql_str, connection);
                int noOfRows = myCommand.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                Log("Delete OneOrderUserEntity <" + e.ToString() + "> orderId <" + orderId + ">");
            }
            finally
            {
                if (connection != null && connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
        }

        private void DeleteDbObjectById(string tableName, string dbId, SqlConnection connection)
        {
            try
            {
                if (connection != null && connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }
                string sql_str = "DELETE FROM " + tableName + " WHERE Id='" + dbId + "'";
                SqlCommand myCommand = new SqlCommand(sql_str, connection);
                int noOfRows = myCommand.ExecuteNonQuery();
                if (noOfRows != 1)
                {
                    Log("Delete " + tableName + " no of deleted rows in db <" + noOfRows + "> dbid <" + dbId + ">");
                }
                //Console.WriteLine("Deleted <" + noOfRows + "> MWL_Order for order <" + orderId + ">");
            }
            catch (Exception e)
            {
                Log("Delete" + tableName +" <" + e.ToString() + "> dbid <" + dbId + ">");
            }
            finally
            {
                if (connection != null && connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
        }

        private void DeleteSynchronizationPropertyMetadata(string entityId, SqlConnection connection)
        {
            try
            {
                if (connection != null && connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }
                string sql_str_file = "DELETE FROM PMC_Synchronization_Property_Metadata WHERE EntityId='" + entityId + "'";
                SqlCommand myCommand_file = new SqlCommand(sql_str_file, connection);
                myCommand_file.ExecuteNonQuery();
            }catch(Exception e)
            {
                Log("DeleteSynchronizationPropertyMetadata <" + e.ToString() + ">");
            }
            finally
            {
                if (connection != null && connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
        }

        private void DeleteSynchronizationPropertyMetadata_order(string orderId, List<string> orderReports, List<string> orderFileIds, SqlConnection connection)
        {
            try
            {
                foreach (string fileId in orderFileIds)
                {
                    DeleteSynchronizationPropertyMetadata(fileId, connection);
                }
                foreach (string reportId in orderReports)
                {
                    DeleteSynchronizationPropertyMetadata(reportId, connection);
                }
                DeleteSynchronizationPropertyMetadata(orderId, connection);
            }
            catch (Exception e)
            {
                Log("PMC_Synchronization_Property_Metadata <" + e.ToString() + "> orderId <" + orderId + ">");
            }
        }

        private void DeleteSynchronizationMetadata(string entityId, SqlConnection connection)
        {
            try
            {
                if (connection != null && connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }
                string sql_str_file = "DELETE FROM PMC_Synchronization_Metadata WHERE EntityId='" + entityId + "'";
                SqlCommand myCommand_file = new SqlCommand(sql_str_file, connection);
                myCommand_file.ExecuteNonQuery();
            }catch(Exception e)
            {
                Log("DeleteSynchronizationMetadata <" + e.ToString() + ">");
            }
            finally
            {
                if (connection != null && connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
        }

        private void DeleteSynchronizationMetadata_order(string orderId, List<string> orderReports, List<string> orderFileIds, SqlConnection connection)
        {
            try
            {
                foreach (string fileId in orderFileIds)
                {
                    DeleteSynchronizationMetadata(fileId, connection);
                }
                foreach (string reportId in orderReports)
                {
                    DeleteSynchronizationMetadata(reportId, connection);
                }
                DeleteSynchronizationMetadata(orderId, connection);
            }
            catch (Exception e)
            {
                Log("DeleteSynchronizationMetadata <" + e.ToString() + "> orderId <" + orderId + ">");
            }
        }

        private void DeleteOneOrderLog(string orderId, SqlConnection connection)
        {
            try
            {
                if (connection != null && connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }
                string sql_str = "DELETE FROM OneOrder_Log WHERE OrderEntityId='" + orderId + "'";
                SqlCommand myCommand = new SqlCommand(sql_str, connection);
                int noOfRows = myCommand.ExecuteNonQuery();
                //Console.WriteLine("Deleted <" + noOfRows + "> orderlogs for order <" + orderId + ">");
            }
            catch (Exception e)
            {
                Log("DeleteOneOrderLog <" + e.ToString() + "> orderId <" + orderId + ">");
            }
            finally
            {
                if (connection != null && connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
        }

        private List<String> DeleteOneReportEntity(string orderId, SqlConnection connection)
        {
            try
            {
                if (connection != null && connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }
                List<string> orderReports = getOrderReports(connection, orderId);
                if (orderReports.Count > 0)
                {
                    if (connection != null && connection.State == ConnectionState.Closed)
                    {
                        connection.Open();
                    }
                    string sql_str = "DELETE FROM OneReportEntity WHERE OrderId='" + orderId + "'";
                    SqlCommand myCommand = new SqlCommand(sql_str, connection);
                    int noOfRows = myCommand.ExecuteNonQuery();
                    //Console.WriteLine("Deleted <"+noOfRows+"> reports for order <"+orderId+">");
                }
                return orderReports;
            }
            catch (Exception e)
            {
                Log("DeleteOneReportEntity <" + e.ToString() + "> orderId <" + orderId + ">");
            }
            finally
            {
                if (connection != null && connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return new List<string>();
        }

        private void DeleteFiles(List<string> orderFiles, List<string> orderFileIds, string orderId, SqlConnection connection)
        {
            string baseFilePath = getAppSetting("BaseFilePath", "F:\\precom\\files");
            if (orderId != null && orderId.Length > 2)
            {
                string orderBaseDir = baseFilePath + "\\" + orderId.Substring(0, 2) + "\\";
                string orderFileDir = orderBaseDir + orderId + "\\";
                //Console.WriteLine("orderfile dir <" + orderFileDir + ">");
                try
                {
                    if (connection != null && connection.State == ConnectionState.Closed)
                    {
                        connection.Open();
                    }
                    foreach (string orderFile in orderFiles)
                    {
                        try
                        {
                            //Console.WriteLine("checked if exists <" + orderFileDir + orderFile + ">");
                            if (File.Exists(orderFileDir + orderFile))
                            {
                                //Console.WriteLine("delete <" + orderFileDir + orderFile + ">");
                                File.Delete(orderFileDir + orderFile);
                            }
                            string sql_str = "DELETE FROM MWL_File WHERE OrderId='" + orderId + "' AND Name='" + orderFile + "'";
                            SqlCommand myCommand = new SqlCommand(sql_str, connection);
                            int noOfRows = myCommand.ExecuteNonQuery();

                            //Console.WriteLine("delete MWL_File <" + orderFileDir + orderFile + "> noOfRows <" + noOfRows + ">");

                            if (noOfRows != 1)
                            {
                                Log("DeleteFiles no of deleted rows in db <" + noOfRows + "> File <" + orderFileDir + orderFile + ">");
                            }
                        }
                        catch (Exception e)
                        {
                            Log("DeleteFiles <" + e.ToString() + "> File <" + orderFileDir + orderFile + ">");
                        }
                    }
                    foreach (string orderFileId in orderFileIds)
                    {
                        try
                        {
                            string sql_str = "DELETE FROM OneFileExternalEntity WHERE FileId='" + orderFileId + "'";
                            SqlCommand myCommand = new SqlCommand(sql_str, connection);
                            int noOfRows = myCommand.ExecuteNonQuery();
                            //Console.WriteLine("delete OneFileExternalEntity <" + orderFileId + "> noOfRows <" + noOfRows + ">");
                        }
                        catch (Exception e)
                        {
                            Log("DeleteFiles::FileExternalEntity <" + e.ToString() + "> File <" + orderFileId + ">");
                        }
                    }
                }
                finally
                {
                    if (connection != null && connection.State == ConnectionState.Open)
                    {
                        connection.Close();
                    }
                }
                try
                {
                    if (Directory.Exists(orderFileDir))
                    {
                        if (Directory.GetFiles(orderFileDir).Length < 1)
                        {
                            Directory.Delete(orderFileDir);
                        }
                    }
                    if (Directory.Exists(orderBaseDir))
                    {
                        if (Directory.GetFiles(orderBaseDir).Length < 1)
                        {
                            Directory.Delete(orderBaseDir);
                        }
                    }
                }
                catch (Exception ee)
                {
                    Log("DeleteFiles::OrderFileDir <" + ee.ToString() + "> Dir <" + orderFileDir + ">");
                }
            }
        }

        private List<string> getOrderReports(SqlConnection connection, string orderId)
        {
            List<string> orderReports = new List<string>();
            SqlDataReader myReader = null;
            try
            {
                if (connection != null && connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }
                string sql_str = "SELECT Id FROM OneReportEntity WHERE OrderId='" + orderId + "'";
                SqlCommand myCommand = new SqlCommand(sql_str, connection);
                myReader = myCommand.ExecuteReader();
                while (myReader.Read())
                {
                    orderReports.Add(myReader["Id"].ToString());
                }
            }
            catch (Exception e)
            {
                Log("getOrderReports <" + e.ToString() + "> OrderId <" + orderId + ">");
            }
            finally
            {
                if (connection != null && connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
                if (myReader != null && !myReader.IsClosed)
                {
                    myReader.Close();
                }
            }
            return orderReports;
        }

        private Tuple<List<string>, List<string>> getOrderFiles(SqlConnection connection, string orderId)
        {
            List<string> ordersFileNames = new List<string>();
            List<string> ordersFileIds = new List<string>();
            SqlDataReader myReader = null;
            try
            {
                if (connection != null && connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }
                string sql_str = "SELECT Id, Name FROM MWL_File WHERE OrderId='" + orderId + "'";
                SqlCommand myCommand = new SqlCommand(sql_str, connection);
                myReader = myCommand.ExecuteReader();
                while (myReader.Read())
                {
                    ordersFileNames.Add(myReader["Name"].ToString());
                    ordersFileIds.Add(myReader["Id"].ToString());
                }
            }
            catch (Exception e)
            {
                Log("getOrderFiles <" + e.ToString() + "> OrderId <"+orderId+">");
            }
            finally
            {
                if (connection != null && connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
                if (myReader != null && !myReader.IsClosed)
                {
                    myReader.Close();
                }
            }
            return new Tuple<List<string>, List<string>>(ordersFileNames, ordersFileIds);
        }


        private void calcNewNoOfOrders(DateTime startTime, DateTime stopTime, int noOfOrders)
        {
            try
            {
                int limitSeconds = getLimitSeconds();
                TimeSpan executeTime = stopTime - startTime;
                double execSeconds = Convert.ToInt32(executeTime.TotalMilliseconds / 1000);
                //Console.WriteLine("limitSeconds <" + limitSeconds + "> execSeconds <" + execSeconds + "> noOfOrders <" + noOfOrders + ">");
                if (execSeconds < (limitSeconds - 1) || execSeconds > (limitSeconds + 1))
                {
                    double fakt = (Convert.ToDouble(execSeconds - limitSeconds)) / (Convert.ToDouble(limitSeconds));
                    int adjustment = Convert.ToInt32(noOfOrders * fakt);
                    if (adjustment > noOfOrders)
                    {
                        adjustment = Convert.ToInt32(noOfOrders / 2);
                    }
                    noOfOrders = noOfOrders - adjustment;
                    //Console.WriteLine("new noOfOrders <" + noOfOrders + "> fakt <" + fakt + "> adjustment <"+adjustment+">");
                    setNoOfOrders(noOfOrders);
                }
            }
            catch(Exception e)
            {
                Log("calcNewNoOfOrders <" + e.ToString() + ">");
            }
        }

        private SqlConnection getSqlConnection()
        {
            try
            {
                string connectionString = getAppSetting("ConnectionString", "Server=localhost,1433;DataBase=PreComOne;Min Pool Size=1;Max Pool Size=100;Pooling=True;Integrated security=SSPI");
                return new SqlConnection(connectionString);
            }
            catch (Exception e)
            {
                Log("getSqlConnection <"+e.ToString()+">");
            }
            return null;
        }

        private List<String> getAOsForArchiving(SqlConnection connection, string limitdate)
        {
            List<string> aos = new List<string>();
            SqlDataReader myReader = null;
            try
            {
                if (connection != null && connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }
                string sql_str = "select Id from AONumberEntity where PMC_Row_Status=90 and Active=0 and UpdatedServerUTC<'" + limitdate + "'";
                //Console.WriteLine(sql_str);
                SqlCommand myCommand = new SqlCommand(sql_str, connection);
                myReader = myCommand.ExecuteReader();
                while (myReader.Read())
                {
                    aos.Add(myReader["Id"].ToString());
                }
            }
            catch (Exception e)
            {
                Log("getAOsForArchiving <" + e.ToString() + ">");
            }
            finally
            {
                if (connection != null && connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
                if (myReader != null && !myReader.IsClosed)
                {
                    myReader.Close();
                }
            }
            return aos;
        }

        private List<String> getArticlesForArchiving(SqlConnection connection, string limitdate)
        {
            List<string> articles = new List<string>();
            SqlDataReader myReader = null;
            try
            {
                if (connection != null && connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }
                string sql_str = "select Id from ArticleEntity where PMC_Row_Status=90 and Deleted=1 and UpdatedServerUTC<'" + limitdate + "'";
                //Console.WriteLine(sql_str);
                SqlCommand myCommand = new SqlCommand(sql_str, connection);
                myReader = myCommand.ExecuteReader();
                while (myReader.Read())
                {
                    articles.Add(myReader["Id"].ToString());
                }
            }
            catch (Exception e)
            {
                Log("getArticlesForArchiving <" + e.ToString() + ">");
            }
            finally
            {
                if (connection != null && connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
                if (myReader != null && !myReader.IsClosed)
                {
                    myReader.Close();
                }
            }
            return articles;
        }

        private List<string> getMaterialsForArchiving(SqlConnection connection, string limitdate)
        {
            List<string> materials = new List<string>();
            SqlDataReader myReader = null;
            try
            {
                if (connection != null && connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }
                string sql_str = "select Id from MaterialEntity where PMC_Row_Status=90 and Deleted=1 and UpdatedServerUTC<'" + limitdate + "'";
                //Console.WriteLine(sql_str);
                SqlCommand myCommand = new SqlCommand(sql_str, connection);
                myReader = myCommand.ExecuteReader();
                while (myReader.Read())
                {
                    materials.Add(myReader["Id"].ToString());
                }
            }
            catch (Exception e)
            {
                Log("getMaterialsForArchiving <" + e.ToString() + ">");
            }
            finally
            {
                if (connection != null && connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
                if (myReader != null && !myReader.IsClosed)
                {
                    myReader.Close();
                }
            }
            return materials;
        }

        private List<string> getOrdersForArchiving(SqlConnection connection, string limitdate, int noOfOrders)
        {
            List<string> orders = new List<string>();
            SqlDataReader myReader = null;
            try
            {
                if (connection != null && connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }
                string sql_str = "select top " + noOfOrders + " Id from MWL_Order where StatusTypeId in (16,17) and CreatedDateTime<'" + limitdate + "' order by CreatedDateTime";
                Log("find orders to delete <" + sql_str + ">");
                //Console.WriteLine(sql_str);
                SqlCommand myCommand = new SqlCommand(sql_str, connection);
                myReader = myCommand.ExecuteReader();
                while (myReader.Read())
                {
                    orders.Add(myReader["Id"].ToString());
                }
            }
            catch (Exception e)
            {
                Log("getOrdersForArchiving <" + e.ToString() + ">");
            }
            finally
            {
                if (connection != null && connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
                if (myReader != null && !myReader.IsClosed)
                {
                    myReader.Close();
                }
            }
            return orders;
        }


        private void setNoOfOrders(int noOfOrders)
        {
            File.WriteAllText(getAppSetting("NoOfOrders", "c:\\tmp\\precom_archiving\\NoOfOrders.txt"), "" + noOfOrders, Encoding.UTF8);
        }

        private int getNoOfOrders()
        {
            try
            {
                string noOfOrders_s = File.ReadAllText(getAppSetting("NoOfOrders", "c:\\tmp\\precom_archiving\\NoOfOrders.txt"));
                int noOfOrders = Int32.Parse(noOfOrders_s.Trim());
                if (noOfOrders < 10)
                {
                    return 10;
                }
                return noOfOrders;
            }
            catch (FileNotFoundException)
            {

            }
            catch (Exception e)
            {
                Log("getNoOfOrders() <" + e.ToString() + ">");
            }
            return 50;
        }

        private int getLimitSeconds()
        {
            try
            {
                string limitSeconds_s = getAppSetting("LimitSeconds", "5");
                return Int32.Parse(limitSeconds_s.Trim());
            }
            catch(Exception e)
            {
                Log("getLimitSeconds <" + e.ToString() + ">");
            }
            return 5;
        }

        private void Log(string logMessage)
        {
            string logTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            string logFileDate = DateTime.Now.ToString("yyyy-MM-dd");
            string logFileName = getAppSetting("LogFilePath", "c:\\tmp\\precom_archiving\\") + "log_" + logFileDate + ".txt";
            using (StreamWriter w = File.AppendText(logFileName))
            {
                w.WriteLine(logTime + " : " + logMessage);
                w.WriteLine("-------------------------------");
            }
        }

        private string getAppSetting(string appSetting, string defaultvalue)
        {
            try
            {
                string logFileName = ConfigurationManager.AppSettings[appSetting];
                if (logFileName != null && logFileName.Length > 0)
                {
                    return logFileName;
                }
            }
            catch (Exception e)
            {
                Console.Write(e.Message);
            }
            return defaultvalue;
        }

        static void Main(string[] args)
        {
            ONEPrecomArchiving archiving = new ONEPrecomArchiving();
            archiving.executeArchiving();
            //archiving.testExecute();
        }
    }
}
